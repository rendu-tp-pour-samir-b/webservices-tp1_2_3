var aedes = require('aedes')();
var server = require('net').createServer(aedes.handle);
var port = 8000;

var topic = 'user-test';

server.listen(port, function() {
  
  console.log('Server listening on port', port);

});

aedes.subscribe(topic, function(packet, cb) {
  
  console.log('Published the message =>', packet.payload.toString());

});